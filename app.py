from flask import Flask, session, request, render_template, redirect, url_for
from sqlalchemy import create_engine, and_, or_
from sqlalchemy.orm import sessionmaker
from datetime import date, datetime

import models

# DECLARE APPLICATION
application = Flask(__name__)

# DEFINE SECRET KEY
application.secret_key = 'RQ9TgEMdFiseVmjFEKUV3gF75tG0aH6n'

# CREATE DB SESSION
engine = create_engine(models.SQLALCHEMY_URL)
DBSession = sessionmaker(bind=engine)
db = DBSession()

# DEFINE CUSTOM FUNCTIONS FOR TEMPLATES
# @application.context_processor
# def custom_functions():
#     pass

# ROUTES

@application.route('/login', methods=['GET', 'POST'])
def route_login():
    from hashlib import sha1

    if request.method == 'GET':
        error = request.args.get('error')

        # Cari counter yang masih kosong (belum login)
        subq = db.query(models.Counter_Session.is_available, models.Counter_Session.counter_id).filter(models.Counter_Session.date == date.today()).subquery()
        counters = db.query(models.Counter).outerjoin(subq, subq.c.counter_id == models.Counter.id)\
                    .filter(or_(subq.c.is_available == True, subq.c.is_available == None))\
                    .all()
        
        db.close()
        if error:
            return render_template('login.html', counters=counters, error=error)
        else:    
            return render_template('login.html', counters=counters)

    elif request.method == 'POST':
        # Jika pilihan counter kosong, balik ke login
        if request.form.get('counter_id') == '':
            db.close()
            # return render_template('login.html', error='Kounter tidak dipilih')
            return redirect(url_for('route_login', error='Kounter tidak dipilih'))

        # Cek username dan password
        username = request.form.get('username')
        password = sha1(request.form.get('password')).hexdigest()
        account = db.query(models.Account).filter(models.Account.username == username, models.Account.password == password).first()

        # Jika username/password salah, balik ke login
        if account is None:
            db.close()
            # return render_template('login.html', error='Username/password tidak valid')
            return redirect(url_for('route_login', error='Username/password tidak valid'))

        # Ambil sesi counter yang dipilih
        counter_id = request.form.get('counter_id')
        counter_session = db.query(models.Counter_Session).filter(models.Counter_Session.date == date.today())\
                            .filter(models.Counter_Session.counter_id == counter_id).first()
        
        # Jika counter yang dipilih belum memiliki sesi, buat sesi baru untuk hari ini
        if counter_session is None:
            counter_session = models.Counter_Session(counter_id=counter_id, date=date.today())
            db.add(counter_session)
            db.commit()

        # Selain itu, update sesi counter yang dipilih menjadi tidak tersedia
        else:
            # Jika sedang dipakai balik ke login
            if not counter_session.is_available:
                db.close()
                # return render_template('login.html', error='Counter sedang dipakai')
                return redirect(url_for('route_login', error='Kounter sedang dipakai'))
            else:
                counter_session.is_available = False
                db.commit()

        # Set session untuk account
        session['account_id'] = account.id
        session['username'] = account.username
        session['display_name'] = account.display_name
        session['counter_name'] = db.query(models.Counter.name).filter(models.Counter.id == counter_session.counter_id).first().name
        session['session_id'] = counter_session.id

        # Ke halaman CS
        db.close()
        return redirect(url_for('route_cs'))

@application.route('/logout')
def route_logout():
    # Set sesi counter yang sedang dijalani menjadi tidak tersedia
    counter_session = db.query(models.Counter_Session).filter(models.Counter_Session.id == session['session_id']).first()
    counter_session.is_available = True
    db.commit()

    db.close()
    session.clear()
    return redirect(url_for('route_login'))

@application.route('/cs', methods=['GET', 'POST'])
def route_cs():
    # Jika belum login arahkan ke login
    if 'account_id' not in session:
        return redirect(url_for('route_login'))

    if request.method == 'GET':
        # Ambil data antrian hari ini
        queue = db.query(models.Queue).filter(models.Queue.date == date.today()).first()

        # Jika kosong maka buat antrian baru di hari ini
        if queue is None:
            db.add(models.Queue(date=date.today(), visitor=0))
            db.commit()

            # Masukkan data awal
            number = {
                'visitor' : 0,
                'served' : 0,
                'skip' : ''
            }
            session['current'] = 0
        else:
            number = {}

            # Ambil jumlah antrian pengunjung yang datang
            number['visitor'] = queue.visitor

            # Ambil jumlah pengunjung yang sudah dilayani
            number['served'] = queue.served

            # Ambil nomor yang terlewat
            number['skip'] = ' - '.join(map(str, [num.queue for num in db.query(models.Skipped).filter(models.Skipped.date == date.today()).all()]))

            # Cek sesi counter sedang melayani nomor berapa
            counter_session = db.query(models.Counter_Session).filter(models.Counter_Session.id == session['session_id']).first()
            session['current'] = counter_session.current_queue if counter_session.current_queue else 0

        db.close()
        return render_template('cs.html', number=number)

    elif request.method == 'POST':
        # Tombol berikutnya
        if not request.form.get('btnNext') is None:
            # Cek apakah jumlah visitor = yang dilayani. Jika iya, batalkan proses
            queue = db.query(models.Queue).filter(models.Queue.date == date.today(), models.Queue.visitor == models.Queue.served).first()
            if not queue is None:
                db.close()
                return redirect(url_for('route_cs'))

            # Tambah nomor antrian yang terakhir dilayani
            queue = db.query(models.Queue).filter(models.Queue.date == date.today()).first()
            queue.served += 1

            # Masukkan nomor ke dalam sesi counter yang sedang melayani
            counter_session = db.query(models.Counter_Session).filter(models.Counter_Session.id == session['session_id']).first()
            counter_session.current_queue = queue.served

            # Masukkan nomor ke dalam service/pelayanan
            counter_service = models.Counter_Service(session_id=session['session_id'],account_id=session['account_id'],time=datetime.now(),queue=queue.served)
            db.add(counter_service)

            # Update database
            db.commit()

            # Masukkan ke dalam session sebagai sedang dilayani
            session['current'] = queue.served
        
        # Tombol lewati
        elif not request.form.get('btnSkip') is None:
            # Cek apakah jumlah visitor = yang dilayani. Jika iya, batalkan proses
            queue = db.query(models.Queue).filter(models.Queue.date == date.today(), models.Queue.visitor == models.Queue.served).first()
            if not queue is None:
                db.close()
                return redirect(url_for('route_cs'))

            # Hapus nomor dari service/pelayanan
            counter_service = db.query(models.Counter_Service).filter(models.Counter_Service.session_id == session['session_id'], models.Counter_Service.queue == session['current']).delete()
            db.commit()

            # Masukkan nomor ke dalam tabel dilewati
            db.add(models.Skipped(date=date.today(), queue=session['current']))
            db.commit()

            # Panggil nomor berikutnya
            queue = db.query(models.Queue).filter(models.Queue.date == date.today()).first()
            queue.served += 1

            # Masukkan nomor ke dalam sesi counter yang sedang melayani
            counter_session = db.query(models.Counter_Session).filter(models.Counter_Session.id == session['session_id']).first()
            counter_session.current_queue = queue.served

            # Masukkan nomor ke dalam service/pelayanan
            counter_service = models.Counter_Service(session_id=session['session_id'],account_id=session['account_id'],time=datetime.now(),queue=queue.served)
            db.add(counter_service)

            db.commit()

            # Masukkan ke dalam session sebagai sedang dilayani
            session['current'] = queue.served

        # Tombol panggil terlewat
        elif not request.form.get('btnCallSkip') is None:
            skipped = db.query(models.Skipped).filter(models.Skipped.date == date.today()).first()
            if skipped is None:
                db.close()
                return redirect(url_for('route_cs'))

            # Ambil nomor yang terlewat, masukkan ke dalam sesi counter yang sedang melayani
            counter_session = db.query(models.Counter_Session).filter(models.Counter_Session.id == session['session_id']).first()
            counter_session.current_queue = skipped.queue
            
            # Masukkan nomor ke dalam service/pelayanan
            counter_service = models.Counter_Service(session_id=session['session_id'],account_id=session['account_id'],time=datetime.now(),queue=skipped.queue)
            db.add(counter_service)

            session['current'] = skipped.queue

            # Hapus yang telah diambil
            db.query(models.Skipped).filter(models.Skipped.date == date.today(), models.Skipped.queue == session['current'])\
                .delete()
            db.commit()

        db.close()
        return redirect(url_for('route_cs'))

@application.route('/visitor', methods=['GET', 'POST'])
def route_visitor():
    if request.method == 'GET':
        # Ambil data antrian pada hari ini
        queue = db.query(models.Queue).filter(models.Queue.date == date.today()).first()

        # Jika kosong maka buat antrian baru di hari ini
        if queue is None:
            db.add(models.Queue(date=date.today(), visitor=0))
            db.commit()

            # Save nomor antrian 0 ke session
            session['visitor'] = 0
        else:
            session.visitor = queue.visitor
        
        db.close()
        return render_template('visitor.html')

    elif request.method == 'POST':
        # Tambah nilai berikutnya
        if not request.form.get('btnGet') is None:
            queue = db.query(models.Queue).filter(models.Queue.date == date.today()).first()
            queue.visitor += 1
            db.commit()
            db.close()

        return redirect(url_for('route_visitor'))

@application.route('/display')
def route_display():
    # Ambil antrian untuk setiap cs
    cs_session = {}
    for row in db.query(models.Counter.name, models.Counter_Session.current_queue).outerjoin(models.Counter_Session, models.Counter.sessions)\
        .filter((models.Counter_Session.date == date.today()) | (models.Counter_Session.date == None))\
        .all():

        cs_session.update({row.name : row.current_queue if row.current_queue else 0})

    return render_template('display.html', cs_session=cs_session)


#################################

# MAIN
if __name__ == "__main__":
    application.run(host='0.0.0.0',port=8212)
