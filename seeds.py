DATA = {
    'Account' : [
        {'id':1, 'username':'user1', 'display_name':'User Satu', 'email':'user1@antrian.com', 'password':'6367c48dd193d56ea7b0baad25b19455e529f5ee', 'salt':'12345','account_group_id':2},
        {'id':2, 'username':'user2', 'display_name':'User Dua', 'email':'user2@antrian.com', 'password':'6367c48dd193d56ea7b0baad25b19455e529f5ee', 'salt':'12345','account_group_id':2},
        {'id':3, 'username':'user3', 'display_name':'User Tiga', 'email':'user3@antrian.com', 'password':'6367c48dd193d56ea7b0baad25b19455e529f5ee', 'salt':'12345','account_group_id':2}
    ],
    'Account_Group' : [
        {'id':1, 'name':'Admin'},
        {'id':2, 'name':'User'}
    ],
    'Counter' : [
        {'id':1, 'name':'CS 1'},
        {'id':2, 'name':'CS 2'}
    ]
}