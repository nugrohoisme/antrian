database = [
    {
        "drivername": "postgresql+pg8000",
        "host": "localhost",
        "port": "5432",
        "username": "postgres",
        "password": "1479",
        "database": "procurement"
    },
    {
        "drivername": "mysql+mysqlconnector",
        "host": "localhost",
        "port": None,
        "username": "root",
        "password": None,
        "database": "antrian"
    },
    {
        "drivername": "sqlite",
        "username": None,
        "password": None,
        "database": "db.sqlite"
    }
]

database_default = 2

database_url = None