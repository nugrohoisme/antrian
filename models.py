from sqlalchemy import Column, ForeignKey, Integer, BigInteger, SmallInteger, String, Text, Date, DateTime, Boolean
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.engine.url import URL
from sqlalchemy.orm import relationship, backref
from sqlalchemy import create_engine

import config

SQLALCHEMY_URL = URL(**config.database[config.database_default]) if config.database_url is None else config.database_url

Base = declarative_base()

class Queue(Base):
    __tablename__ = 'queue'
    sqlite_autoincrement=True

    id = Column(Integer, primary_key=True)
    date = Column(Date, nullable=False)
    visitor = Column(Integer, nullable=False)
    served = Column(Integer, nullable=False, server_default='0')

class Skipped(Base):
    __tablename__ = 'skipped'

    date = Column(Date, primary_key=True)
    queue = Column(Integer, primary_key=True)

class Counter(Base):
    __tablename__ = 'counter'
    sqlite_autoincrement = True

    id = Column(SmallInteger, primary_key=True)
    name = Column(String(15), nullable=False)

    sessions = relationship('Counter_Session', backref='counter')

class Counter_Session(Base):
    __tablename__ = 'counter_session'
    sqlite_autoincrement = True

    id = Column(Integer, primary_key=True)
    counter_id = Column(SmallInteger, ForeignKey('counter.id', ondelete='SET NULL'))
    date = Column(Date, nullable=False)
    current_queue = Column(Integer)
    is_available = Column(Boolean, nullable=False, server_default='0')

    services = relationship('Counter_Service', backref='session')

class Counter_Service(Base):
    __tablename__ = 'counter_service'
    sqlite_autoincrement = True
    
    id = Column(BigInteger().with_variant(Integer, "sqlite"), primary_key=True)
    session_id = Column(Integer, ForeignKey('counter_session.id', ondelete='CASCADE'), nullable=False)
    account_id = Column(Integer, ForeignKey('account.id', ondelete='SET NULL'))
    time = Column(DateTime, nullable=False)
    queue = Column(Integer)

class Account(Base):
    __tablename__ = 'account'
    sqlite_autoincrement=True

    id = Column(Integer, primary_key=True)
    username = Column(String(100), unique=True, nullable=False)
    display_name = Column(String(250), nullable=False)
    email = Column(String(100), unique=True, nullable=False)
    password = Column(String(40), nullable=False)
    salt = Column(String(13), nullable=False)
    is_login = Column(Boolean, nullable=False, server_default='0')
    account_group_id = Column(Integer, ForeignKey('account_group.id', onupdate='CASCADE', ondelete='SET NULL'))

    metas = relationship('Account_Meta', backref='account', cascade="all, delete-orphan", passive_deletes=True)
    services = relationship('Counter_Service', backref='account')

class Account_Meta(Base):
    __tablename__ = 'account_meta'

    account_id = Column(Integer, ForeignKey('account.id', onupdate='CASCADE', ondelete='CASCADE'), primary_key=True)
    name = Column(String(20), primary_key=True)
    value = Column(Text, nullable=False)

class Account_Group(Base):
    __tablename__ = 'account_group'
    sqlite_autoincrement=True

    id = Column(Integer, primary_key=True)
    name = Column(String(20), nullable=False)
    
    accounts = relationship('Account', backref='account_group')

if __name__ == '__main__':
    print 'Create engine...'
    engine = create_engine(SQLALCHEMY_URL)
    print 'Create database...'
    Base.metadata.create_all(engine)
    print 'Finished!'
